package com.zimap.test.network;

import com.zimap.test.model.AnimalListModel;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static final String BASE_URL="https://kot3.com/";

    private Api apiRequests;
    private static ApiClient getClient = null;

    private ApiClient(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient networkClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(networkClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.apiRequests= retrofit.create(Api.class);
    }


    public static ApiClient getClient() {
        if (getClient == null) {
            getClient = new ApiClient();
        }
        return getClient;
    }

    public Call<AnimalListModel> getAnimals(String animalType){
        return apiRequests.getAnimals(animalType);
    }
}
