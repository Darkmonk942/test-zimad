package com.zimap.test.network;

import com.zimap.test.model.AnimalListModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

    @GET("xim/api.php")
    Call<AnimalListModel> getAnimals(@Query("query") String animalType);
}
