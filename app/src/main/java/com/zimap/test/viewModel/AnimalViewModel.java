package com.zimap.test.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.Parcelable;

import com.zimap.test.model.AnimalListModel;
import com.zimap.test.network.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnimalViewModel extends ViewModel {
    private MutableLiveData<AnimalListModel> animalsData;
    private Parcelable state;

    public LiveData<AnimalListModel> getAnimals(final String animalType){
        if(animalsData==null){
            animalsData=new MutableLiveData<>();
        }

        ApiClient.getClient().getAnimals(animalType).enqueue(new Callback<AnimalListModel>() {
            @Override
            public void onResponse(Call<AnimalListModel> call, Response<AnimalListModel> response) {
                if (response.isSuccessful()){
                    animalsData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<AnimalListModel> call, Throwable t) {

            }
        });

        return animalsData;
    }

    public void saveState(Parcelable state){
        this.state=state;
    }

    public Parcelable restoreState(){
        return state;
    }
}
