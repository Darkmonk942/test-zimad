package com.zimap.test.viewModel;

import android.arch.lifecycle.ViewModel;
import android.os.Bundle;

public class AnimalListViewModel extends ViewModel {
    private int tabPosition;

    public void saveTabPosition(int tabPosition){
        this.tabPosition=tabPosition;
    }

    public int restoreTabPosition(){
        return tabPosition;
    }
}
