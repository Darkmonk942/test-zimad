package com.zimap.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnimalModel {
    public static final String TYPE_ANIMAL_CAT="cat";
    public static final String TYPE_ANIMAL_DOG="dog";

    @SerializedName("url")
    @Expose
    public String imageUrl;

    @SerializedName("title")
    @Expose
    public String title;
}
