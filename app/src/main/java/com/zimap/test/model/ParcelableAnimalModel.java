package com.zimap.test.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableAnimalModel implements Parcelable {
    public static final String PARCELABLE_ANIMAL="parcelable_animal";
    public int position;
    public String urlImg;
    public String title;

    public ParcelableAnimalModel(int position,String urlImg,String title){
        this.position=position;
        this.urlImg=urlImg;
        this.title=title;
    }

    private ParcelableAnimalModel(Parcel in) {
        this.position=in.readInt();
        this.urlImg=in.readString();
        this.title=in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.position);
        parcel.writeString(this.urlImg);
        parcel.writeString(this.title);
    }

    public static final Creator<ParcelableAnimalModel> CREATOR = new Creator<ParcelableAnimalModel>() {
        @Override
        public ParcelableAnimalModel createFromParcel(Parcel in) {
            return new ParcelableAnimalModel(in);
        }

        @Override
        public ParcelableAnimalModel[] newArray(int size) {
            return new ParcelableAnimalModel[size];
        }
    };
}
