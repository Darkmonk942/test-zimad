package com.zimap.test.activity;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zimap.test.R;
import com.zimap.test.databinding.ActivityMainScreenBinding;

public class AnimalListActivity extends AppCompatActivity {
    private ActivityMainScreenBinding activityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding=DataBindingUtil.setContentView(this,R.layout.activity_main_screen);
    }
}
