package com.zimap.test.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.zimap.test.R;
import com.zimap.test.databinding.FragmentAnimalDetailsBinding;
import com.zimap.test.model.ParcelableAnimalModel;

public class AnimalDetailFragment extends Fragment {
    private FragmentAnimalDetailsBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_animal_details, container, false);

        setAnimalInfo(getArguments());
        return binding.getRoot();
    }

    private void setAnimalInfo(Bundle bundle){
        if(bundle!=null){
            ParcelableAnimalModel animalModel=bundle.getParcelable(ParcelableAnimalModel.PARCELABLE_ANIMAL);
            binding.textCounter.setText(String.valueOf(animalModel.position));
            binding.textDetails.setText(animalModel.title);

            Picasso.get().load(animalModel.urlImg).into(binding.imgAnimal);
        }
    }
}
