package com.zimap.test.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zimap.test.R;
import com.zimap.test.adapter.AnimalListAdapter;
import com.zimap.test.model.AnimalListModel;
import com.zimap.test.model.AnimalModel;
import com.zimap.test.viewModel.AnimalViewModel;

import java.util.ArrayList;

public class DogFragment extends Fragment {
    public static final String BUNDLE_DOG="dog";

    private AnimalViewModel animalViewModel;
    private ArrayList<AnimalModel> animals = new ArrayList<>();
    private AnimalListAdapter animalListAdapter;
    private RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        animalViewModel=ViewModelProviders.of(this).get(AnimalViewModel.class);
        animalListAdapter=new AnimalListAdapter(animals);

        if(bundle!=null){
            animalViewModel.saveState(bundle.getParcelable(BUNDLE_DOG));
        }
        getAnimals();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_dogs, container, false);
        initRecycler(recyclerView, animalViewModel.restoreState());
        return recyclerView;
    }

    private void initRecycler(RecyclerView recyclerView,Parcelable state) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(animalListAdapter);
        if(state!=null){
            recyclerView.getLayoutManager().onRestoreInstanceState(state);
        }
    }

    private void getAnimals() {
        animalViewModel.getAnimals(AnimalModel.TYPE_ANIMAL_DOG).observe(this, new Observer<AnimalListModel>() {
            @Override
            public void onChanged(@Nullable AnimalListModel animalListModel) {
                if (animalListModel != null) {
                    animals.clear();
                    animals.addAll(animalListModel.animals);
                    animalListAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(BUNDLE_DOG,recyclerView.getLayoutManager().onSaveInstanceState());
        super.onSaveInstanceState(outState);
    }
}