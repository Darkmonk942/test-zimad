package com.zimap.test.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zimap.test.R;
import com.zimap.test.databinding.FragmentAnimalsBinding;
import com.zimap.test.viewModel.AnimalListViewModel;

public class AnimalListFragment extends Fragment {
    private FragmentManager fragmentManager;
    private FragmentAnimalsBinding binding;

    private AnimalListViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        fragmentManager=getChildFragmentManager();
        viewModel=ViewModelProviders.of(this).get(AnimalListViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_animals, container, false);
        initTabs(binding.tabs);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        if(viewModel.restoreTabPosition()==0){
            FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
            fragmentTransaction.hide(fragmentManager.findFragmentById(R.id.fragment_dog));
            fragmentTransaction.commit();
        }

    }

    private void initTabs(final TabLayout tabLayout){
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                switch (tab.getPosition()){
                    case 0:
                        fragmentTransaction.hide(fragmentManager.findFragmentById(R.id.fragment_dog));
                        fragmentTransaction.show(fragmentManager.findFragmentById(R.id.fragment_cat));
                        break;
                    case 1:
                        fragmentTransaction.hide(fragmentManager.findFragmentById(R.id.fragment_cat));
                        fragmentTransaction.show(fragmentManager.findFragmentById(R.id.fragment_dog));
                        break;
                }
                fragmentTransaction.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tabLayout.getTabAt(viewModel.restoreTabPosition()).select();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        viewModel.saveTabPosition(binding.tabs.getSelectedTabPosition());
        super.onPause();
    }
}
