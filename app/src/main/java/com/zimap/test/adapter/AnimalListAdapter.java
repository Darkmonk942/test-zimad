package com.zimap.test.adapter;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.zimap.test.R;
import com.zimap.test.databinding.ItemRecyclerAnimalBinding;
import com.zimap.test.model.AnimalModel;
import com.zimap.test.model.ParcelableAnimalModel;

import java.util.ArrayList;

import androidx.navigation.Navigation;

public class AnimalListAdapter extends RecyclerView.Adapter<AnimalListAdapter.ViewHolder> {
    private ArrayList<AnimalModel> animals;

    public AnimalListAdapter(ArrayList<AnimalModel> animals){
        this.animals=animals;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ItemRecyclerAnimalBinding binding;

        public ViewHolder(ItemRecyclerAnimalBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }

    @NonNull
    @Override
    public AnimalListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemRecyclerAnimalBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_recycler_animal, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimalListAdapter.ViewHolder holder, int position) {
        AnimalModel animal=animals.get(position);

        holder.binding.setAnimal(animal);
        holder.binding.textCounter.setText(String.valueOf(position+1));
        Picasso.get().load(animal.imageUrl).into(holder.binding.icAnimal);

        holder.binding.getRoot().setTag(position);
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateDetailScreen(view);
            }
        });
    }

    private void navigateDetailScreen(View view){
        int position=(Integer)view.getTag();
        AnimalModel animal=animals.get(position);
        ParcelableAnimalModel parcelableAnimal=new ParcelableAnimalModel(position+1,animal.imageUrl,animal.title);
        Bundle bundle=new Bundle();
        bundle.putParcelable(ParcelableAnimalModel.PARCELABLE_ANIMAL,parcelableAnimal);
        Navigation
                .findNavController(view)
                .navigate(R.id.action_garden_fragment_to_plant_detail_fragment,bundle);
    }

    @Override
    public int getItemCount() {
        return animals.size();
    }
}
